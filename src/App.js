import React, {Component} from 'react';
import Button from './Components/Button/Button'
import './App.scss';
import Modal from './Components/Modal/Modal'

class App extends Component {
  state = {
    firstModal: false,
    secondModal: false,
  }

  showFirstModal = () => {
    this.setState({firstModal: true});
  }
  showSecondModal = () => {
    this.setState({secondModal: true});
  }

  closeModal = (event) => {
    this.setState({firstModal: false})
    this.setState({secondModal: false})
  }

  render() {
    return (
      <div className="App">
        <div className={"buttons__section"}>
          <Button text={"Open first modal"} backgroundColor={"teal"}
                  onClick={() => this.showFirstModal()}/>
          <Button text={"Open second modal"} backgroundColor={"green"}
                  onClick={() => this.showSecondModal()}/>
        </div>
        {this.state.firstModal ?
          <Modal header={"First Modal"} text={"FIRST Modal, we did it)"} closeButton={true} actions={
            <>
              <button className={'modal__buttons-btn'}>Ok</button>
              <button className={'modal__buttons-btn'}>Cancel</button>
            </>
          } onClick={(event) => this.closeModal(event)}/> : ''}
        {this.state.secondModal ?
          <Modal header={"Second Modal"} text={"This is our amazing SECOND Modal"} closeButton={false} actions={
            <>
              <button className={'modal__buttons-btn'}>Apply</button>
              <button className={'modal__buttons-btn'}>Cancel</button>
              <button className={'modal__buttons-btn'}>Change</button>
            </>
          } onClick={(event) => event.currentTarget === event.target && this.closeModal()}/> : ''}
      </div>
    )
  }
}

export default App;