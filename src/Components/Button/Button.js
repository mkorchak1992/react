import React, {Component} from 'react';
import './Button.scss'

class Button extends Component {

  render() {
    const {text, backgroundColor, onClick} = this.props;

    return (
      <button className={'buttons__section-btn'} onClick={onClick} style={{backgroundColor: backgroundColor}}>{text}</button>
    );
  }
}

export default Button;